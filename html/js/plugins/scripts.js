//GLOBAL VARIABLE (DECLARE variable in caps ex: var MY_VAR = '';)

//LOAD / RESIZE / SCROLL
$(window).on('load resize', function(){
	$('#contentBody').height($(window).height()-155);
	$('.home-page-parent').height($(window).height()-155);
	$('.masters-menu-style').height($(window).height()-125);
	//collapsed menu active 1024width
	if($(window).width()<=1024){
		$("body").addClass('leftpanel-collapsed');
		$('.nav-parent').removeClass('nav-active');
			if ($body.hasClass('leftpanel-collapsed'))
            $('.leftpanel-collapsed').find('ul.children').hide();
		
		}else{
			$("body").removeClass('leftpanel-collapsed');
			}
	
		
});
//DOCUMENT READY
$(function(){
	var modalBodyHeight = $(window).height()-111;
	$('[data-modal="right"]').on('show.bs.modal', function () {
       $(this).find('.modal-dialog').css({
              width:$(this).attr('modal-width') ? $(this).attr('modal-width') : 'auto'
       });
	   //modal-scrool height
	   $(this).find('.modal-scroll').css({
		   /*'height':'auto',
			'max-height':modalBodyHeight*/
		   'height':modalBodyHeight,
		   'overflow-y':'auto'
		});
	});
	//Materialize select
	$('select').material_select();
	//Materialize Date picker
	$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15 // Creates a dropdown of 15 years to control year
  });
  
  	$( ".select-dropdown" ).change(function() {
 		$(this).parent('.select-wrapper').find('.select-label').show();
	});
	$('.datepicker').click(function(){
	var datePickerValue = $('.datepicker').val();
		if(datePickerValue !== ""){
			$('.datepicker').prev().addClass('active');
		}
	});
	/*$('#myModal').on('shown.bs.modal', function () {
		 	$('.modal').modal({
            keyboard: false,
            backdrop: 'static'
        });
	})*/
	 
	
  	//malihu scroll
   $(window).load(function(){
      $("#contentBody").mCustomScrollbar({
  		theme:"dark",
		autoHideScrollbar: true,
		autoExpandScrollbar: true
		});
		$(".modal-scrol").mCustomScrollbar({
  		theme:"dark",
		autoHideScrollbar: true,
		autoExpandScrollbar: true,
		updateOnContentResize: true 
		});
   });
   
});

//USER DEFINED FUNCTIONS
