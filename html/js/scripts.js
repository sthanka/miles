var _menusUl = $('#left-panel-main-menus-ul'), _searchLi, _menuSearchBox, _inputSearch;
function selectMenu(){
    //_menusUl.html('');

    var leftMenuData = JSON.parse(jsonString);
    var list =[], obj={};
    $.each(leftMenuData, function() {
        if(this.submenu == ""){
            $('<li><a href="'+this.url+'"><i style="width:18px;overflow:hidden" class="fa '+this.icon+'"></i><span>' + this.name + '</span></a></li>').appendTo($(_menusUl));
        }
        else{
            var subMenuLi = '';
            subMenuLi += '<li class="nav-parent"><a href="javascript:;"><i style="width:18px;overflow:hidden" class="fa '+this.icon+'"></i><span>' + this.name + '</span></a><ul class="children">';
            $.each(this.submenu, function(i, o){
                subMenuLi += '<li><a href="'+ o.url +'"><i class="fa fa-caret-right"></i><span>'+o.name+'</span></a></li>' ;
            });
            subMenuLi +='</ul></li>';
            $('#left-panel-main-menus-ul').append(subMenuLi);

        }
    });

    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseleave');
    $('.leftpanel .nav-bracket > li.nav-hover').unbind('mouseenter');
    $('.leftpanel .nav-parent > a').unbind('click');
    $('.leftpanel .nav-bracket > li').mouseenter(function (event) {
        var cur = $(this);
        var halfLPHt = parseInt(($(window).height() - 80) / 2);
        if ($body.hasClass('leftpanel-collapsed')) {
            cur.addClass('nav-hover');
            if (cur.offset().top < halfLPHt) {
                cur.find('ul.children').show().css({ 'top': cur.offset().top + cur.height() });
            } else {
                cur.find('ul.children').show().css({ 'top': cur.offset().top - cur.find('ul.children').height() + 8 });
            }
            $("li.nav-hover ul.children").mCustomScrollbar({ theme: "light", scrollbarPosition: 'outside', autoHideScrollbar: true, scrollButtons: { enable: true }, scrollInertia: 3, autoExpandScrollbar: true });
        }
    }).mouseleave(function (event) {
        var cur = $(this);
        cur.removeClass('nav-hover');
        cur.find('ul.children').css({ 'top': '' });
        if ($body.hasClass('leftpanel-collapsed'))
            cur.find('ul.children').hide();
    });
    $('.leftpanel .nav-parent > a').click(function () {
        var parent = $(this).parent();
        var sub = parent.find('> ul');
        if (!$('body').hasClass('leftpanel-collapsed')) {
            if (sub.is(':visible')) {
                sub.slideUp(200, function () {
                    parent.removeClass('nav-active active');
                    $('.mainpanel').css({ height: '' });
                });
            } else {
                $('.leftpanel .nav-parent').each(function () {
                    var t = $(this);
                    if (t.hasClass('nav-active')) {
                        t.find('> ul').slideUp(200, function () {
                            t.removeClass('nav-active');
                        });
                    }
                });
                $('.nav-bracket > li').removeClass('nav-active active')
                parent.addClass('nav-active');
                sub.slideDown(200);
            }
        }
    });
    preSelectMenu();

    $body = $('body');
    $('body[data-theme="change"] .logo-change').css('margin-left', -($('body[data-theme="change"] .logo-change')).width() / 2)
// Page Preloader
    $('#body-status').fadeOut();
    $('#preloader').fadeOut();
    $leftPan = $('.leftpanel'), $mainPan = $('.mainpanel'), $mainWA = $('.main-work-area');
    $('.menutoggle').click(function () {
        var cur = $(this);
        if ($body.hasClass('leftpanel-collapsed')) {
            $body.removeClass('leftpanel-collapsed');
            $('.nav-active-sm').addClass('nav-active').removeClass('nav-active-sm').find('ul.children').show();
        } else {
            $body.addClass('leftpanel-collapsed');
            $('.nav-active').addClass('nav-active-sm').removeClass('nav-active').find('ul.children').hide();
        }
    });
}


function preSelectMenu() {
    var url = decodeURIComponent(window.location.href);
    url=url.split('/')[url.split('/').length-1];
    $("a[href$='" + url + "']").closest('li').parent().parent().addClass('nav-active');
    $("a[href$='" + url + "']").closest('li').parent().css('display','block');
    $("a[href$='" + url + "']").closest('li').addClass('active');
}