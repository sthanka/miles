'use strict';
    angular.module('app', [
        'ui.router'
    ])

    .config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'partials/login.html'
            })
            .state('app',{
                templateUrl: 'partials/layout.html'
            })
            .state('app.default', {
                url: '/index',
                templateUrl: 'partials/default.html'
            })
            .state('app.batch-info', {
                url: '/batch-info',
                templateUrl: 'partials/batch-info.html',
                controller:'batchInfoCtrl'
            })
            .state('app.batches-list', {
                url: '/batches-list',
                templateUrl: 'partials/batches-list.html'
            })
            .state('app.branch-info', {
                url: '/branch-info',
                templateUrl: 'partials/branch-info.html'
            })
            .state('app.branches-list', {
                url: '/branches-list',
                templateUrl: 'partials/branches-list.html'
            })
            .state('app.fee-collection', {
                url: '/fee-collection',
                templateUrl: 'partials/fee-collection.html'
            })
            .state('app.masters', {
                url: '/masters',
                templateUrl: 'partials/masters.html'
            })
            .state('app.payment', {
                url: '/payment',
                templateUrl: 'partials/payment.html'
            })
            .state('app.welcome', {
                url: '/welcome',
                templateUrl: 'partials/welcome.html'
            })
        }
    ]);












