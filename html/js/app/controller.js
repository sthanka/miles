angular.module('app')

    .controller('AppCtrl', function ($rootScope, $scope) {
        $scope.menuOpened = false;
        $scope.toggleMenu = function() {
            $scope.menuOpened = !($scope.menuOpened);
        };
        window.onclick = function (e) {
            if ($scope.menuOpened) {
                $scope.menuOpened = false;

                $scope.$apply();
            }
        };
    })
    .controller('batchInfoCtrl', function ($rootScope, $scope, $state, $uibModal) {
        $scope.modalOpen = function (sData) {
            var modalInstance = $uibModal.open({
                animation: true,
                keyboard :false,
                windowClass: 'my-class',
                openedClass: 'right-panel-modal modal-open',
                templateUrl: 'batch-list-modal.html',
                resolve: {
                    items: function () {
                        return sData;
                    }
                },
                controller: function ($rootScope, $scope, $uibModalInstance, companyService, items) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {

            });
        };



    })