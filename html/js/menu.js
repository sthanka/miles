// LEFTPANEL MENU

var jsonData = [
	
	//STORE
	
{
    "name": "Account",
    "url": "",
	"icon": "fa-user",
    "submenu": [
    {
        "name": "Fee Collection",
        "url":  "#"
    },
    {
        "name": "Fee Assignment",
        "url":  "#"
    },
	{
        "name": "Concession",
        "url":  "#"
    }
	
    ]
},
	
{
    "name": "Configuration",
    "url": "",
	"icon": "fa-user",
    "submenu": [
    {
        "name": "Fee Collection",
        "url":  + "#"
    },
    {
        "name": "Fee Assignment",
        "url":  + "#"
    }
    ]
},
{
    "name": "Crm Reports",
    "url": "#",
	"icon": "fa-gear",
    "submenu": [
        {
        "name": "Feehead",
        "url":  "#/masters"
    }
    ]
},
{
    "name": "Masters",
    "url":"",
	"icon": "fa-gear",
    "submenu": [
    {
        "name": "Feehead",
        "url":  "#/masters"
    },
    {
        "name": "Fee Company",
        "url":  + "#"
    },
    {
        "name": "Course Fee",
        "url":  + "#"
    },
	{
        "name": "Fee Types",
        "url":  + "#"
    },
	{
        "name": "Branches",
        "url":  + "#"
    },
	{
        "name": "Batches",
        "url":  + "#"
    }
    ]
},
{
    "name": "Batches",
    "url":"",
	"icon": "fa-gear",
    "submenu": [
    {
        "name": "Batches List",
        "url":  "#/batches-list"
    },
    {
        "name": "Batch Info",
        "url":  "#/batch-info"
    }
    ]
},
{
    "name": "Branches",
    "url":"",
	"icon": "fa-gear",
    "submenu": [
    {
        "name": "Branches List",
        "url":  "#/branches-list"
    },
    {
        "name": "Branch Info",
        "url":  "#/branch-info"
    }
    ]
}
];
var jsonString = JSON.stringify(jsonData);
