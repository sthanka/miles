<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class References extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("References_model");
    }

	public function GetReferences_get()
	{
        $res=$this->References_model->selectUser();
        $this->response($res, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
	}
    public function AddReferenceValue_post()
    {
        $reference_type_id=$this->post('reference_type_id');
        $reference_type_val=$this->post('reference_type_val');
        $reference_type_val_parent=$this->post('reference_type_val_parent');
    }
}
