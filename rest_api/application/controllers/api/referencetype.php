<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

//require APPPATH . '/controllers/common.php';  /*Using for common functionalities*/

/**
 *
 * @controller Name Configuration
 * @category        Controller
 * @author          Abhilash
 */

class Referencetype extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->model("Reference_type_model");
        
//        $this->commonFunctions = new Common();
    }
    
    /**
     * Function for geting Country details and child
     * 
     * @args 
     *  "id"(INT) -> id for configuration [NOT Required]
     * 
     * @return - array
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    public function country_get() {
        $data = $this->input->get();
        
        $result = $this->Reference_type_model->getAllCountry($data);
        
        if($result == 'dberror'){
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        }else{
            $this->result_set('200', 'Success - 200', $result);
        }
    }
    
    /**
     * Function for geting all details
     * 
     * @args 
     *  "id"(INT) -> id for configuration [NOT Required]
     * 
     * @return - array
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    public function referencetype_get() {
        $data = $this->input->get();
        
        $result = $this->Reference_type_model->getAllCountry($data);
        
        if($result == 'dberror'){
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        }else{
            $this->result_set('200', 'Success - 200', $result);
        }
    }
    
    /**
     * function for geting state details and child (City)
     * 
     * @args 
     *  "reference_type_id"(INT) -> id for State
     * 
     * @return - array
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    public function state_get() {
        $data = $this->input->get();
        
        $result = $this->Reference_type_model->getState($data);
        
        if($result == 'dberror'){
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        }else if($result === 'nostate'){
            $this->result_set('400', 'Error - 400', 'No State found');
        }else if($result){
            $this->result_set('200', 'Success - 200', $result);
        }else {
            $this->result_set('400', 'Error - 400', 'Something went wrong, please try after seomtime.');
        }
    }

    /**
     * function for geting City details
     * 
     * @args 
     *  "reference_type_id"(INT) -> id for City
     * 
     * @return - array
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    public function city_get() {
        $data = $this->input->get();
        
        $result = $this->Reference_type_model->getAllCity($data);
        if($result == 'dberror'){
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        }else if($result){
            $this->result_set('200', 'Error - 200', $result);
        }else{
            $this->result_set('200', 'Error - 200', 'No data found');
        }
        
        
    }

    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 19 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - Error - 400, Success - 200, .. etc
     * @param type - String/Array, $data 
     * 
     * #auther - Abhilash
     */
    public function result_set($status, $msg, $data) {
        /*$resp_array = ['200', '201'];
        $statusVal = 'FALSE';
        if (in_array($status, $resp_array)) {
            $statusVal = 'TRUE';
            $status = "HTTP/1.0 " . $status . " SUCCESS";
        } else {
            $status = "HTTP/1.1 " . $status . " ERROR";
        }

        $result = ['status' => $statusVal, 'message' => $msg, 'data' => $data];
        $this->output->set_header($status);
        $this->output->set_output(json_encode($result));*/
        $this->response([
            'status' => $status,
            'message' => $msg,
            'data' => $data,
                ], $status);
    }

}
