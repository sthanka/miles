<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userauthentication extends CI_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();
        $this->load->model("Userauthentication_model");
        $this->load->library('form_validation');
    }

    function userauthentication() {
        
        $this->form_validation->set_rules('email_id', 'email_id', 'required|regex_match[/\S+@\S+\.\S+/]', array('required' => 'User mail id sholud not be blank', 'regex_match' => 'Invalid email id'));
        
        $this->form_validation->set_rules('user_name', 'user_name', 'regex_match[/^[A-z]+$/]', array('regex_match' => 'Invalid User Name'));
        
        $this->form_validation->set_rules('phone_number', 'phone_number', 'regex_match[/^(\+\d{1,3}[- ]?)?\d{10}$/]', array('regex_match' => 'Invalid Phone Number'));
        
        $this->form_validation->set_rules('user_dob', 'user_dob', "regex_match[/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/]", array('regex_match' => 'Invalid Date of Birth. Date should be in YYYY-MM-DD Format.'));
        
        
        
        

        if ($this->form_validation->run() == FALSE) {
            $this->result_set('400', 'Bad Request', $this->form_validation->error_array());
        }
        else{

        $user_email_id = $this->input->post("email_id");
        $user_name = $this->input->post("user_name");
        $phone_number = $this->input->post("phone_number");
        $profile_image = $this->input->post("profile_image");
        $user_dob = $this->input->post("user_dob");
        
        $oauth_id = $this->input->post("accesstoken");

        $data['user_email_id'] = $user_email_id;
        $data['user_name'] = $user_name;
        $data['phone_number'] = $phone_number;
        $data['profile_image'] = $profile_image;
        $data['user_dob'] = $user_dob;
        $data['accesstoken'] = $oauth_id;
        $res = $this->Userauthentication_model->checkUserExist($data);
       
       if($res['status']===true){
          
           $res['data'] = $this->createToken($res['data']['id'], $res['data']['email'], $res['data']['role']);
          
           
           $this->result_set('200', $res['message'], $res['data']);
         //  print_r($res);die("asd");
           
       }
       else{
           $this->result_set('400', $res['message'], $res['data']);
       }
       
        }
    }

    private function createToken($id, $email, $role) {
        // Set the POST data
        $client_id = $id;

        $this->load->model("Users_model");

        $secret_id = $this->Users_model->getsecret_id($client_id);

        if ($secret_id == 'dberror') {
            $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
        } else {
            $client_scopes = $this->Users_model->getscopes($client_id);

            if ($client_scopes == 'dberror') {
                $this->result_set('400', 'Error - 400', '[DB]Something went wrong, Plase try after sometime.');
            } else {
                
                $scopeids = '';
                foreach ($client_scopes as $scopeid) {
                    $scopeids.=$scopeid->scope_id . ',';
                }

                $scopeids = rtrim($scopeids, ',');
                
                $postdata = http_build_query(
                        array(
                            'client_id' => $client_id,
                            'client_secret' => $secret_id,
                            'grant_type' => 'client_credentials',
                            'scope' => $scopeids
                        )
                );

                // Set the POST options
                $opts = array('http' =>
                    array(
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n",
                        'content' => $postdata
                    )
                );

                // Create the POST context
                $context = stream_context_create($opts);

                // POST the data to an api
                $url = BASE_URL . 'index.php/api/login/sign';
                $actoken = (file_get_contents($url, false, $context));
                $actoken = json_decode($actoken);
                
                if (isset($actoken)) {
                    $response['access_token'] = $actoken->access_token;
                    $response['token_type'] = $actoken->token_type;
                    $response['expires_in'] = $actoken->expires_in;
                    $response['id'] = $id;
                    $response['email'] = $email;
                    $response['role'] = $role;

                    return $response;
                } else {
                    $this->result_set('401', 'Error - 401', 'Access Denied!!');
                }
            }
        }
    }

    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - Error - 400, Success - 200, .. etc
     * @param type - String/Array, $data 
     * 
     * #auther - Abhilash
     */
    public function result_set($status, $msg, $data) {
        $resp_array = ['200', '201'];
        $statusVal = 'FALSE';
        if (in_array($status, $resp_array)) {
            $statusVal = 'TRUE';
            $status = "HTTP/1.0 " . $status . " SUCCESS";
        } else {
            $status = "HTTP/1.1 " . $status . " ERROR";
        }

        $result = ['status' => $statusVal, 'message' => $msg, 'data' => $data];
       
        $this->output->set_header($status);
        $this->output->set_output(json_encode($result));
        
      // print_r($result);
         // exit;
        /*$this->response([
            'status' => $status,
            'message' => $msg,
            'data' => $data,
                ], $status);*/
    }

}

?>
