<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
//class Country extends REST_Controller {
class Referencetype extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->model("Reference_type_model");
    }

    public function all_get() {
        $result = array();
        $res = $this->Reference_type_model->getAllCountry();
        if ($res) {
            foreach ($res as $key => $val) {
                if ($val->parent_id === NULL) {
                    //No parent
                    $tmp = explode(',', $val->child);
                    foreach ($tmp as $k => $v) {
                        $result[$val->reference_type_id][] = $this->checkParent($v);
                    }
                } else {
                    //contains child
                    $tmp = explode(',', $val->child);
                    foreach ($tmp as $k => $v) {
                        $result[$val->reference_type_id][] = $this->checkParent($v);
                    }
                }
            }
            print_r($result);die;
        }else{
            echo 'blank';
        }
    }

    public function checkParent($id) {
        $individualRes = $this->Reference_type_model->getIndividual($id);
        if ($individualRes) {
            foreach($individualRes as $key=>$val){
                if($val->parent_id===NULL){
                    return array(
                            /*'id' => $val->parent_id,*/
                            'name' => $val->value,
                            'is_active' => $val->is_active
                          );
                }else{
                    $childIndividualRes = $this->Reference_type_model->getIndividual($id);
//                    print_r($childIndividualRes);die;
                    if ($childIndividualRes) {
//                        echo 'child';
                        return array(
                            'id' => $val->parent_id,
                            'name' => $val->value,
                            'is_active' => $val->is_active
                          );
                    }else{
                        return array();
                    }
                }
            }
            /*print_r($individualRes);
            die;
            $result[] = array(
                'id' => $individualRes['parent_id'],
                'name' => $individualRes['name']
            );
            print_r($result);*/
        } else {
            echo 'blank';
        }
    }
    
    

    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //$this->form_validation->set_rules('id', "ID", 'callback_name_check');
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
