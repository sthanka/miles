<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class Employee extends REST_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->database();
        $this->load->model("Employee_model");
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->library('oauth');
        
    }
function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}
    public function employee_get(){
        
        
        $is_having_scope=$this->oauth->checkscope('Employeelist');
        
        if($is_having_scope===true){
            
        $employe_id=$this->get('id');
        $search_key=$this->get('search_key');
        $search_value=$this->get('search_value');
        $data['employe_id']=$employe_id;
        $data['search_key']=$search_key;
        $data['search_value']=$search_value;
        
        $this->form_validation->set_data($this->get());

        $validations[]=["field"=>"id","label"=>"id","rules"=>"integer","errors"=>array(
        'required' => 'Employee ID sholud not be blank','integer' => 'Employee ID sholud be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
       
         
        if($validationstatus===true){
            
        $this->load->library('pagination');
        
        $config['base_url'] = BASE_URL.'index.php/api/employee/employee/page/';
        $total_count=$this->Employee_model->getEmployeeCount($data);
        
        if($total_count['status']){
            $total_rows=$total_count['data']['count'];
        }
        else{
            $total_rows=0;
        }
        
        
        $config['total_rows'] = $total_rows;
        $config['per_page'] = 3;
        $config['display_pages'] = FALSE;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 4;
        
        

        $this->pagination->initialize($config);
        $pagination_links= $this->pagination->create_links();
        $page = $this->get('page') ? $this->get('page') : 0;
        $res=$this->Employee_model->getEmployeeList($data,$config["per_page"], $page);
        
        
        if($res['status']===true){
        $res_row['rows']=$res['data'];
        $pagination['first']=$this->pagination->first_url_param;
        $pagination['last']=$this->pagination->last_url_param;
        $pagination['previous']=$this->pagination->previous_url_param;
        $pagination['next']=$this->pagination->next_url_param;
        }
        else{
            $pagination=array();
        }
        $final_response['response']=[
            'status'=>$res['status'],
            'message' => $res['message'],
            'data'=>$res['data'],
            'links' => $pagination
                ];
            
        $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        
        }
        else{
            
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
            
        }
       }
        else{
            
            $final_response['response']=[
            'status'=>FALSE,
            'message' => 'Access Denied',
            'data'=>array(),
            'links' =>array()
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_NOT_FOUND;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
    }
    public function employee_post(){
        
        
        $is_having_scope=$this->oauth->checkscope('AddEmployee');
        
        if($is_having_scope===true){
        $employee_code=$this->post('employee_code');
        $employee_mailid=$this->post('employee_mailid');
      
        //$this->form_validation->set_data($this->post());
        $validations[]=["field"=>"employee_code","label"=>"employee_code","rules"=>"required|regex_match[/^[A-Za-z0-9]+$/]|is_unique[employee.employee_code]","errors"=>array('required' => "Employee code should not be blank",'regex_match' => 'Invalid Employee code',"is_unique"=>"Emloyee code already exist")];
        
        $validations[]=["field"=>"employee_mailid","label"=>"employee_mailid","rules"=>"required|regex_match[/\S+@\S+\.\S+/]|is_unique[employee.email]","errors"=>array('required' => 'Employee mail id sholud not be blank','regex_match' => 'Invalid email id',"is_unique"=>"Emloyee email already exist")];
        
        $validationstatus=$this->customvalidation($validations);
        
        if($validationstatus===true){
            
            $data['employee_code']=$employee_code;
            $data['employee_mailid']=$employee_mailid;
            $res_addempoyee=$this->Employee_model->addEmployee($data);
            
            $final_response['response']=[
                'status' => $res_addempoyee['status'],
                'message' => $res_addempoyee['message'],
                'data' => $res_addempoyee['data'],
                ];
            
            if($res_addempoyee['status']===true){
                
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            
            }
            else{
                
                $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
            }
            
        }
        else{
            
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Access Denied',
                'data' => array(),
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function employee_delete(){
        
        $is_having_scope=$this->oauth->checkscope('DeleteEmployee');
        
        if($is_having_scope===true){
        
        $employe_id=$this->get('id');
       
        $this->form_validation->set_data(array("id" => $employe_id));

        $validations[]= ["field"=>"id","label"=>"id","rules"=> "required|integer","errors"=>array(
        'required' => 'Employee ID sholud not be blank','integer' => 'Employee ID sholud be integer value only'
        )];
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            
            $res_delete=$this->Employee_model->deleteEmployee($employe_id);
            
                $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
                ];
            
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        }
         else {
             $final_response['response']=[
                'status' => FALSE,
                'message' => 'Access Denied',
                'data' => array(),
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    public function employee_put(){
        
        $is_having_scope=$this->oauth->checkscope('EditEmployee');
        
        if($is_having_scope===true){
        $employee_id=$this->get('id');
        $employee_code=$this->get('employee_code');
        $employee_mailid=$this->get('employee_mailid');
        
        
        
        $this->form_validation->set_data(array("id" => $employee_id,"employee_code" => $employee_code,"employee_mailid" => $employee_mailid));
        
        
        
        $validations[]= ["field"=>"id","label"=>"id","rules"=> "required|integer","errors"=>array(
        'required' => 'Employee ID sholud not be blank','integer' => 'Employee ID sholud be integer value only'
        )];
        
       
        
        $validations[]=["field"=>"employee_code","label"=>"employee_code","rules"=>"required|regex_match[/^[A-Za-z0-9]+$/]|is_unique[employee.employee_code]","errors"=>array('required' => "Employee code should not be blank",'regex_match' => 'Invalid Employee code',"is_unique"=>"Emloyee code already exist")];
        
        
        $validations[]=["field"=>"employee_mailid","label"=>"employee_mailid","rules"=>"required|regex_match[/\S+@\S+\.\S+/]|is_unique[employee.email]","errors"=>array('required' => 'Employee mail id sholud not be blank','regex_match' => 'Invalid email id',"is_unique"=>"Emloyee email already exist")];
         
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data['employee_id']=$employee_id;
            $data['employee_code']=$employee_code;
            $data['employee_mailid']=$employee_mailid;
            $res_update=$this->Employee_model->updateEmployee($data);
            
            $final_response['response']=[
                'status' => $res_update['status'],
                'message' => $res_update['message'],
                'data' => $res_update['data'],
                ];
            
            if($res_update['status']){
                
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
              
                $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
    }
    else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'Access Denied',
                'data' => array(),
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
        
}
}

?>