<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->model("Users_model");
    }

    public function create_post() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'required|alpha_dash');

        if ($this->form_validation->run() == FALSE) {
//            print_r($this->validation_errors());die('Error ');
            $this->response($this->validation_errors(), 400);
        } else {
            $name = $this->input->post('name');

            $isCreated = $this->Users_model->createUser(array('name' => $name));
            if ($isCreated) {
//                header("HTTP/1.1 200 User Created");
                $this->response(array('msg' => "User Created"), 200);
            } else {
//                header("HTTP/1.1 402 Something went wrong");
                $this->response(array('msg' => "Something went wrong"), 400);
            }
        }
    }

    public function delete_get() {

        $this->load->library('form_validation');

        $this->form_validation->set_data(array('id' => $this->get('id')));

        $this->form_validation->set_rules('id', "ID", 'numeric');

        if ($this->form_validation->run() == FALSE) {
            $this->response($this->validation_errors(), 400);
        } else {
            $id = $this->get('id');
            
            $userStatus = $this->Users_model->getUserStatus($id);

            $isDeleted = $this->Users_model->deleteUser(array('id' => $id, 'status'=> !$userStatus->status));
            if ($isDeleted) {
//                header("HTTP/1.1 200 User Deleted");
                if($userStatus->status){
                    $this->response(array('msg' => "User Deleted"), 200);
                }else{
                    $this->response(array('msg' => "User Active"), 200);
                }
            } else {
//                header("HTTP/1.1 402 Something went wrong");
                $this->response(array('msg' => "Something went wrong"), 400);
            }
        }
    }

    public function edit_post() {

        $id = $this->get('id');

        $this->load->library('form_validation');
        $this->form_validation->set_data(array('id' => $this->get('id')));
        $this->form_validation->set_rules('id', "ID", 'numeric');

        if ($this->form_validation->run() == FALSE) {
            $this->response($this->validation_errors(), 400);
        } else {
            $this->form_validation->set_rules('name', "Name", 'alpha_dash');
            $this->form_validation->set_rules('role', "Role", 'numeric');

            if ($this->form_validation->run() == FALSE) {
                $this->response($this->validation_errors(), 400);
            } else {
                $name = $this->post('name');
                $role = $this->post('role');

                if (strlen($role) <= 0 && strlen($name) <= 0) {
                    $this->response(array('msg' => "Can't be left blank"), 400);
                }
                $data = array();
                if (strlen($role) > 0) {
                    if (preg_match("/^[12]$/", $role)) {
                        $data['role'] = $role;
                    } else {
//                    header("HTTP/1.1 400 Role is invalid");
                        $this->response(array('msg' => "Role is invalid"), 400);
                    }
                }

                if (strlen($name) > 0) {
                    if (strlen($name) > 0 && preg_match("/^[a-zA-Z ]*$/", $name)) {
                        $data['name'] = $name;
                    } else {
                        $this->response(array('msg' => "Name is invalid"), 400);
                    }
                }

                if (count($data) > 0) {
                    $data['id'] = $id;
                    $isUpdated = $this->Users_model->updateUser($data);
                    if ($isUpdated) {
                        $this->response(array('msg' => 'Profile Updated'), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array('msg' => "Something went wrong, please try after sometime"), 400);
                    }
                }
            }
        }

        /* if (strlen($id) > 0 && preg_match("/^[0-9]*$/", $id)) {
          //Valid
          $this->load->library('form_validation');




          } else {
          $this->response("Bad Request", 400);
          } */
    }

    public function all_get() {
        $id = $this->get('id');
        $limit = $this->get('limit');
        $start = $this->get('start');

        if (strlen($id) >= 1 && preg_match("/^[0-9]*$/", $id)) {
            //Valid id
            $this->load->library('form_validation');

            $this->form_validation->set_data(array('id' => $this->get('id')));
            $this->form_validation->set_rules('id', "ID", 'callback_num_check');

            if ($this->form_validation->run() == FALSE) {
                $this->response($this->validation_errors(), 400);
            } else {
                if (strlen($limit) < 0 && preg_match("/^[0-9]*$/", $limit)) {
                    $limit = NULL;
                }

                if (strlen($start) < 0 && preg_match("/^[0-9]*$/", $start)) {
                    $start = NULL;
                }

                $allData = $this->Users_model->getAllUser($id, $start, $limit);

                if ($allData) {
                    $this->response($allData, 200);
                } else {
                    $this->response(array('msg' => "Something went wrong, please try after sometime"), 400);
                }
            }
        } else {
            
            $tot = $this->get('cnt');
            
            //no id - means all
            $this->load->helper("url"); /*Required*/
            $this->load->library('pagination'); /*Required*/
            
            $allData = $this->Users_model->getAllUser(NULL, $start, $limit);
            
            /*This can be set at config.php*/
            $config["base_url"] = base_url() . "index.php/api/user/all/";
            $config["total_rows"] = count($allData);
            $config["per_page"] = $tot?$tot:'5';
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);

            $this->pagination->initialize($config);

            $page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;   /*Requires to change the segement a/c to URL*/
            $data["results"] = $this->Users_model->paginatedUser($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            
            $this->response($data, 200);
            
            /*-------------------OR--------------------*/
            
            /*if (strlen($limit) < 0 && preg_match("/^[0-9]*$/", $limit)) {
                $limit = NULL;
            }

            if (strlen($start) < 0 && preg_match("/^[0-9]*$/", $start)) {
                $start = NULL;
            }

            $allData = $this->Users_model->getAllUser(NULL, $start, $limit);
            if ($allData) {
                $this->response($allData, 200);
            } else {
                $this->response(array('msg' => "Something went wrong, please try after sometime"), 400);
            }*/
        }
    }

    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //$this->form_validation->set_rules('id', "ID", 'callback_name_check');
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
