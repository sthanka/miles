<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Index extends CI_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->library('form_validation');
    }

    public function register() {
        $this->form_validation->set_rules('fullname', 'Name', 'required|alpha');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]'
                , array('is_unique' => 'This Email is already registered, please use another one.'));
        $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['error' => validation_errors()]);
        } else {
            $this->load->model("Users_model");
            $isregistered = $this->Users_model->registeruser($this->input->post());
            if ($isregistered) {
//                header('Content-Type: application/json');
//                json_encode(array('message'=>'success'));
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode(array(
                        'message' => 'Resistered Successfully',
                        'type' => 'success'
                )));
            } else {
//                header('Content-Type: application/json');
//                json_encode(array('message' => 'Wrong Credentials'));
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(400)
                    ->set_output(json_encode(array(
                            'message' => 'Resgistration failed',
                            'type' => 'danger'
                )));
            }
        }
    }

    public function login() {
        $data = array();
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $this->load->model("Users_model");
        $response = $this->Users_model->loginUser($data);
        $scopeids = '';
        if (!$response) {

            $response['status'] = "invalid login";
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(400)
                ->set_output(json_encode(array(
                        'message' => 'Wrong Credentials',
                        'type' => 'danger'
            )));
        } else {

            $client_id = $response[0]->id;
            $secret_id = $this->Users_model->getsecret_id($client_id);

            $client_scopes = $this->Users_model->getscopes($client_id);

            foreach ($client_scopes as $scopeid) {
                $scopeids.=$scopeid->scope_id . ',';
            }

            $scopeids = rtrim($scopeids, ',');
        }

        // Set the POST data
        $postdata = http_build_query(
                array(
                    'client_id' => $client_id,
                    'client_secret' => $secret_id,
                    'grant_type' => 'client_credentials',
                    'scope' => $scopeids
                )
        );


        // Set the POST options
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "User: $client_id\r\n",
                'content' => $postdata
            )
        );


        // Create the POST context
        $context = stream_context_create($opts);


        // POST the data to an api
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/api/index/sign';
//        $url = 'http://localhost.rest.com/index.php/api/index/sign';
        $actoken = (file_get_contents($url, false, $context));
        $response['access_token_response'] = $actoken['access_token_response'];
//        echo json_encode($response);
        if($actoken){
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode(array(
                        'message' => 'Login successfully',
                        'type' => 'success',
                        'access_token_response' => $actoken
            )));
        }else{
            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(400)
                ->set_output(json_encode(array(
                        'message' => 'Login Failed',
                        'type' => 'warning'
            )));
        }
    }

    public function sign() {

        $this->load->library('oauth/oauth', '', 'oauth');
        $oauth = $this->oauth;
        $accesstoken = $oauth->generateAccessToken();
//        echo json_encode($accesstoken);
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                    'message' => 'Login successfully',
                    'type' => 'success',
                    'access_token_response' => $accesstoken
        )));
    }

}
