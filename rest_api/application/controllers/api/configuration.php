<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 *
 * @controller Name Configuration
 * @category        Controller
 * @author          Abhilash
 */
class Configuration extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();

        $this->load->database();

        $this->load->model("app_configuration_model");

        $this->load->library('form_validation');
        
        $this->load->library('oauth');

        $this->load->library('put_method_extenstion');   /* method PUT */

//        $this->commonFunctions = new Common_function();
//        $this->load->library('Common_function');
    }

    /**
     * Function for geting configuration settings list and specific configuration settings)
     * 
     * @args 
     *  "id"(INT) -> id for configuration [NOT Required]
     * 
     * @return - array
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    function configuration_get() {

        $is_having_scope = $this->oauth->checkscope('configurationlist');
        if(!$is_having_scope){
             $this->result_set('401', 'Error - 401', 'Access Denied');
        }

        $id = $this->get('id');
        if (strlen($id) >= 1 && preg_match("/^[0-9]*$/", $id)) {
            //when id is present
            $this->form_validation->set_data(array('id' => $id));
            $this->form_validation->set_rules('id', "ID", 'callback_num_check');

            if ($this->form_validation->run() == FALSE) {

                $this->result_set('400', 'Error - 400', $this->form_validation->error_array());
            } else {

                $result = $this->app_configuration_model->getConfiguration($id);
                if($result == 'dberror') {
                    $this->result_set('400', 'Error - 400', 'Something went wrong, Plase try after sometime.');
                }else if ($result) {
                    $this->result_set('200', 'Success - 200', $result);
                } else {
                    $this->result_set('400', 'Error - 400', 'Something went wrong, Plase try after sometime.');
                }
            }
        } else {
            //no id
            $result = $this->app_configuration_model->getConfiguration();
            
            if($result == 'dberror') {
                $this->result_set('400', 'Error - 400', 'Something went wrong, Plase try after sometime.');
            }else if ($result) {
                $this->result_set('200', 'Success - 200', $result);
            } else {
                $this->result_set('400', 'Error - 400', 'Something went wrong, Plase try after sometime.');
            }
        }
    }

    /**
     * Function adding new site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function configuration_post() {
        
        $is_having_scope = $this->oauth->checkscope('configurationPost');
        if(!$is_having_scope){
             $this->result_set('401', 'Error - 401', 'Access Denied');
        }

        $config = array(
            array(
                'field' => 'key',
                'label' => 'key',
                'rules' => 'required|is_unique[`app_configuration_setting`.`key`]|callback_name_check'
            ),
            array(
                'field' => 'value',
                'label' => 'value',
                'rules' => 'callback_name_check'
            )
        );

        $this->form_validation->set_rules($config);

        $this->form_validation->set_data($this->input->post());

        if ($this->form_validation->run() == FALSE) {
            
            $this->result_set('400', 'Error - 400', $this->form_validation->error_array());
        } else {
            
            $result = $this->app_configuration_model->addConfiguration($this->input->post('key'), $this->input->post('value'));
            
            if ($result === 'dberror') {
                
                $this->result_set('400', 'Error - 400', '[DB] Something went wrong, Plase try after sometime.');
            } else if ($result === 'duplicate') {
                
                $this->result_set('400', 'Error - 400', 'This Key is already in use, please use another one.');
            } else if ($result) {
                
                $this->result_set('200', 'Success - 200', 'Data Saved');
            } else {
                
                $this->result_set('400', 'Success - 400', 'Something went wrong, Plase try after sometime.');
            }
        }
    }

    /**
     * Function Updateing existing site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     *  "id"(INT) -> congiguration ID
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function configuration_put() {
        
        $is_having_scope = $this->oauth->checkscope('configurationPut');
        if(!$is_having_scope){
             $this->result_set('401', 'Error - 401', 'Access Denied');
        }
        
        $id = $this->get('id');

        if (strlen($id) >= 1 && preg_match("/^[0-9]*$/", $id)) {

            $this->form_validation->set_data(array('id' => $id));
            $this->form_validation->set_rules('id', "ID", 'callback_num_check');

            if ($this->form_validation->run() == FALSE) {
                
                $this->result_set('400', 'Error - 400', $this->form_validation->error_array());
            } else {

                $this->put = file_get_contents('php://input');
                //parse and clean request
                $this->put_method_extenstion->_parse_request($this->put);
                
                $config = array(
                    array(
                        'field' => 'key',
                        'label' => 'key',
                        'rules' => 'required|is_unique[`app_configuration_setting`.`key`]|callback_name_check'
                    ),
                    array(
                        'field' => 'value',
                        'label' => 'value',
                        'rules' => 'callback_name_check'
                    )
                );
                
                $this->form_validation->set_rules($config);

                $this->form_validation->set_data($this->put());

                if ($this->form_validation->run() == FALSE) {
                    
                    $this->result_set('400', 'Error - 400', $this->form_validation->error_array());
                } else {
                    
                    $result = $this->app_configuration_model->editConfiguration($id, $this->put("key"), $this->put("value"));

                    if($result === 'dberror'){
                        
                        $this->result_set('400', 'Error - 400', '[DB] Something went wrong, Please try after sometime.');
                    }else if ($result === 'duplicate') {
                        
                        $this->result_set('400', 'Error - 400', 'This Key is already in use, please use another one.');
                    } else if ($result === 'badrequest') {
                        
                        $this->result_set('400', 'Error - 400', 'No Id Found.');
                    } else if ($result) {
                        
                        $this->result_set('200', 'Success - 200', 'Data Updated successfully.');
                    } else {
                        
                        $this->result_set('400', 'Error - 400', 'Something went wrong, Please try after sometime.');
                    }
                }
            }
        } else {
            
            $this->result_set('400', 'Error - 400', 'Bad Request.');
        }
    }

    /**
     * Function deleting existing site configuration
     * 
     * @args 
     *  "key"(String) -> key[POST]
     *  "value"(String) -> value[POST]
     *  "id"(INT) -> congiguration ID
     * 
     * @return - (Bool)
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function configuration_delete() {

        $id = $this->get('id');

        $this->form_validation->set_data(array('id' => $id));
        $this->form_validation->set_rules('id', "id", 'callback_num_check');

        if ($this->form_validation->run() == FALSE) {
            
            $this->result_set('400', 'Error - 400', $this->form_validation->error_array());
        } else {

            $result = $this->app_configuration_model->deleteConfiguration($id);

            if ($result === 'dberror') {

                $this->result_set('400', 'Error - 400', '[DB] Something went wrong, Plase try after sometime.');
            } else if ($result === 'active') {

                $this->result_set('200', 'Success - 200', 'Setting Activated');
            } else if ($result === 'deactivated') {
                
                $this->result_set('200', 'Success - 200', 'Setting De-Activated');
            } else if ($result === 'nodata') {
                
                $this->result_set('400', 'Error - 400', 'Wrong id');
            } else {
                
                $this->result_set('400', 'Error - 400', 'Something went wrong, Plase try after sometime.');
            }
        }
    }

    /**
     * Function for checking number(REGEX)
     * 
     * @args 
     *  "string"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function num_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^[0-9]*$/", $str)) {
            $this->form_validation->set_message('num_check', 'The %s field is not valid!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function for checking valid string
     * 
     * @args 
     *  "str"(String) -> any string
     * 
     * @return - Bool
     * @date modified - 20 Jan 2016
     * @author : Sam
     */
    public function name_check($str) {
        if (strlen($str) <= 0 || !preg_match("/^([a-zA-Z ])/", $str)) {
            $this->form_validation->set_message('name_check', 'The %s field is not valid!, use only Alphabets');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function Name - result_set
     * @param type String, $status EX - 200, 400,.. etc
     * @param type String, $msg Ex - Error - 400, Success - 200, .. etc
     * @param type - String/Array, $data 
     * 
     * #auther - Abhilash
     */
    public function result_set($status, $msg, $data) {
        /* $resp_array = ['200','201'];
          if(in_array($status, $haystack)){

          } */
        $this->response([
            'status' => $status,
            'message' => $msg,
            'data' => $data,
                ], $status);
    }

}
