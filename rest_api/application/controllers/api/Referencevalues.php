<?php
defined('BASEPATH') OR exit('No direct script access allowed');





require APPPATH . '/libraries/REST_Controller.php';

class Referencevalues extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->database();
        $this->load->model("References_model");
        $this->load->library('form_validation');
        $this->load->library('oauth');
    }
    function customvalidation($data){
   
    $this->form_validation->set_rules($data);
    
    if ($this->form_validation->run() == FALSE)
        {
           if(count($this->form_validation->error_array())>0){
               return $this->form_validation->error_array();
           }
           else{
               return true;
           }
        }
        else{
            return true;
        }
    
}

    /*public function GetReferences_get()
    {

        $res=$this->References_model->GetReferences();

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);


       


    }*/
    public function referencevalues_get(){
        
        $is_having_scope=$this->oauth->checkscope('Employeelist');
        
        if($is_having_scope===true){
            
            $reference_type_id=$this->get("reference_type_id");
            $this->form_validation->set_data(array("reference_type_id" => $reference_type_id));
        
        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => 'Reference ID sholud not be blank','integer' => 'Reference ID sholud be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
        
         if($validationstatus===true){
            $res=$this->References_model->getReferenceValuesList($reference_type_id);
            if($res['status']===true){
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
            }
            $result = array('status'=>$res['status'], 'message' => $res['message'], 'data'=>$res['data']);
            $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
            
            
            
         }
         else{
             
             $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
            
         }
        
        }
        else{
            $final_response['response']=[
            'status'=>FALSE,
            'message' => 'Access Denied',
            'data'=>array(),
            'links' =>array()
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_NOT_FOUND;
        }
        
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    public function referencevalue_get(){

         $reference_type_id=$this->get("reference_type_id");
         $parent_id=$this->get("parent_id");

        
       $this->form_validation->set_data(array("parent_id" => $this->get('parent_id'),"reference_type_id" => $this->get('reference_type_id')));

       
       
       $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => 'Reference ID sholud not be blank','integer' => 'Reference ID sholud be integer value only'
        )];
       
       $validations[]=["field"=>"parent_id","label"=>"parent_id","rules"=>"integer","errors"=>array(
        'integer' => 'Reference ID sholud be integer value only'
        )];
        
        $validationstatus=$this->customvalidation($validations);
       if($validationstatus===true){
           
        $res=$this->References_model->getReferenceValues($reference_type_id,$parent_id);

        if($res['status']===true){
            $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
        }
        else{
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        
        $final_response['response']=[
                'status' => $res['status'],
                'message' => $res['message'],
                'data' => $res['data'],
                ];
        
       }else{
           $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
       }
        


        
        $this->response($final_response['response'], $final_response['responsehttpcode']);


    }
    public function fields_get(){

        $reference_type_id=$this->get("reference_type_id");
        $this->form_validation->set_data(array("reference_type_id" => $this->get('reference_type_id')));

        $this->form_validation->set_rules('reference_type_id', 'reference_type_id', 'required|integer',array(
        'required' => 'Reference ID sholud not be blank','integer' => 'Reference ID sholud be integer value only'
        ));
        if ($this->form_validation->run() == FALSE)
        {


            foreach($this->form_validation->error_array() as $formerrors_key => $formerrors_value){


            $this->response([
            'status' => FALSE,
            'message' => $formerrors_value,
            'data' => '',
            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

            }


        }
        $res=$this->References_model->getAddReferenceValueFileds($reference_type_id);


        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function referencevalues_post(){

        

        $reference_type_id=$this->post('reference_type_id');
        $reference_type_val=$this->post('reference_type_val');
        $reference_type_is_active=$this->post('reference_type_is_active');
        
        $reference_type_val_parent=empty(trim($this->post('reference_type_val_parent'))) ? '' : trim($this->post('reference_type_val_parent')) ;


        $reftype=$this->References_model->GetReferences($reference_type_id);
        $error_label="Reference";
        foreach($reftype as $rowrefurenece){

            $error_label=$rowrefurenece->name;

        }

        /* $reftype_values=$this->References_model->GetReferenceValues($reference_type_val_parent);
        foreach($reftype_values as $rowrefurenece_values){

            $parent_reftypeid=$rowrefurenece_values->parent_id;

            $reftype_parent=$this->References_model->GetReferences($parent_reftypeid);

            foreach($reftype_parent as $rowrefurenece_parent){

            $error_label_parent=$rowrefurenece->name;

            }

        } */

        

        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        )];
        
       
        $validations[]=["field"=>"reference_type_val","label"=> $error_label.' Value',"rules"=>"required|regex_match[/^[A-Za-z,\']+$/]","errors"=>array(
        'required' => $error_label.' Value sholud not be blank','regex_match' => $error_label.' Value sholud be alphabetic only'
        )];

        
        $validations[]=["field"=>"reference_type_val_parent","label"=> 'reference_type_val_parent',"rules"=>"integer","errors"=>array('integer' => 'Parent ID sholud be integer value only')];

        


        
        
        $validations[]=["field"=>"reference_type_is_active","label"=> 'reference_type_is_active',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array(
'required' => $error_label.' Status sholud not be blank','integer' => $error_label.' Status sholud be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
)];
        
        $validationstatus=$this->customvalidation($validations);
        if($validationstatus===true){
            $data=array();
            $data['reference_type_id']=$reference_type_id;
            $data['reference_type_val']=$reference_type_val;
            $data['reference_type_val_parent']=$reference_type_val_parent;
            $data['reference_type_is_active']=$reference_type_is_active;


            $res_add=$this->References_model->AddReferenceValue($data);
            $final_response['response']=[
            'status'=>$res_add['status'],
            'message' => $res_add['message'],
            'data'=>$res_add['data']];
            
             if($res_add['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;
            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_NOT_FOUND;
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);

    }
    public function referenceUpdate_post(){


        $reference_type_val_id=$this->post('reference_type_val_id');
        $reference_type_id=$this->post('reference_type_id');
        $reference_type_val=$this->post('reference_type_val');
        $reference_type_is_active=$this->post('reference_type_is_active');

        $reference_type_val_parent=empty(trim($this->post('reference_type_val_parent'))) ? '' : trim($this->post('reference_type_val_parent'));
        $error_label="Reference";
         $reftype=$this->References_model->GetReferences($reference_type_id);
        foreach($reftype as $rowrefurenece){

            $error_label=$rowrefurenece->name;

        } 


        /* $reftype_values=$this->References_model->GetReferenceValues($reference_type_val_parent);
        foreach($reftype_values as $rowrefurenece_values){

            $parent_reftypeid=$rowrefurenece_values->parent_id;

            $reftype_parent=$this->References_model->GetReferences($parent_reftypeid);

            foreach($reftype_parent as $rowrefurenece_parent){

            $error_label_parent=$rowrefurenece->name;

            }

        } */

        $validations[]=["field"=>"reference_type_val_id","label"=>"reference_type_val_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        )];
        
        
        $validations[]=["field"=>"reference_type_id","label"=>"reference_type_id","rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        )];
        
        
        $validations[]=["field"=>"reference_type_val","label"=>$error_label.' Value',"rules"=>"required|regex_match[/^[A-Za-z,\']+$/]","errors"=>array(
        'required' => $error_label.' Value sholud not be blank','regex_match' => $error_label.' Value sholud be alphabetic only'
        )];

        
        $validations[]=["field"=>"reference_type_val_parent","label"=>'reference_type_val_parent',"rules"=>"integer","errors"=>array('integer' => 'Parent ID sholud be integer value only')];

        
        $validations[]=["field"=>"reference_type_is_active","label"=>'reference_type_is_active',"rules"=>"required|integer|regex_match[/^[0-1]$/]","errors"=>array(
'required' => $error_label.' Status sholud not be blank','integer' => $error_label.' Status sholud be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
)];

        $validationstatus=$this->customvalidation($validations);
       
         
        if($validationstatus===true){
            $data=array();

            $data['reference_type_id']=$reference_type_id;
            $data['reference_type_val']=$reference_type_val;
            $data['reference_type_val_parent']=$reference_type_val_parent;
            $data['reference_type_is_active']=$reference_type_is_active;
            $data['reference_type_val_id']=$reference_type_val_id;
            $res_update=$this->References_model->UpdateReferenceValue($data);
            $final_response['response']=[
                'status' => $res_update['status'],
                'message' => $res_update['message'],
                'data' => $res_update['data'],
                ];
            
            
            if($res_update['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;

            }
            else{
                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_NOT_FOUND;
                
            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->response($final_response['response'], $final_response['responsehttpcode']);
    }
    public function referencevalues_delete(){

        $error_label="Reference";

        $reference_type_val_id=$this->get('reference_type_val_id');

        $this->form_validation->set_data(array("reference_type_val_id" => $this->get('reference_type_val_id')));

        
        
        $validations[]=["field"=>"reference_type_val_id","label"=>'reference_type_val_id',"rules"=>"required|integer","errors"=>array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        )];

        $validationstatus=$this->customvalidation($validations);
       
         
        if($validationstatus===true){
            
            $data['reference_type_val_id']=$reference_type_val_id;
            $res_delete=$this->References_model->RemoveReferenceValue($data);
            
            $final_response['response']=[
                'status' => $res_delete['status'],
                'message' => $res_delete['message'],
                'data' => $res_delete['data'],
                ];
            
            
            
            if($res_delete['status']===true){
                //sucess
                $final_response['responsehttpcode']=REST_Controller::HTTP_OK;

            }
            else{

                //fail
                $final_response['responsehttpcode']=REST_Controller::HTTP_NOT_FOUND;
                

            }
        }
        else{
            $final_response['response']=[
                'status' => FALSE,
                'message' => 'validation failed',
                'data' => $validationstatus,
                ];
            
            $final_response['responsehttpcode']=REST_Controller::HTTP_BAD_REQUEST;
        }

         $this->response($final_response['response'], $final_response['responsehttpcode']);


        

    }
}
