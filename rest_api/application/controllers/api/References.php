<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Referencevalues extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        $this->load->database();
        $this->load->model("References_model");
        $this->load->library('form_validation');
    }

    public function GetReferences_get()
    {

        $res=$this->References_model->GetReferences();

        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);


       


    }
    public function referencesvalues_get(){

        $reference_type_id=$this->get("reference_type_id");
        $res=$this->References_model->getReferenceValuesList($reference_type_id);


        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);


    }
    public function referenceValue_get(){

        $reference_type_id=$this->get("reference_type_id");
        $parent_id=$this->get("parent_id");
        $res=$this->References_model->getReferenceValues($reference_type_id,$parent_id);


        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);


    }
    public function getAddReferenceValueFileds_get(){

        $reference_type_id=$this->get("reference_type_id");
        $res=$this->References_model->getAddReferenceValueFileds($reference_type_id);


        $result = array('status'=>TRUE, 'message' => 'success', 'data'=>$res);
        $this->response($result, REST_Controller::HTTP_OK);

    }
    public function reference_post(){

        

        $reference_type_id=$this->post('reference_type_id');

        $reference_type_val_parent=empty(trim($this->post('reference_type_val_parent'))) ? '' : trim($this->post('reference_type_val_parent')) ;


        $reftype=$this->References_model->GetReferences($reference_type_id);
        $error_label="Reference";
        foreach($reftype as $rowrefurenece){

            $error_label=$rowrefurenece->name;

        }

        /* $reftype_values=$this->References_model->GetReferenceValues($reference_type_val_parent);
        foreach($reftype_values as $rowrefurenece_values){

            $parent_reftypeid=$rowrefurenece_values->parent_id;

            $reftype_parent=$this->References_model->GetReferences($parent_reftypeid);

            foreach($reftype_parent as $rowrefurenece_parent){

            $error_label_parent=$rowrefurenece->name;

            }

        } */

        

        
        $this->form_validation->set_rules('reference_type_id', 'reference_type_id', 'required|integer',array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        ));


        $this->form_validation->set_rules('reference_type_val', $error_label.' Value', 'required|regex_match[/^[A-Za-z,\']+$/]',array(
        'required' => $error_label.' Value sholud not be blank','regex_match' => $error_label.' Value sholud be alphabetic only'
        ));


        $this->form_validation->set_rules('reference_type_val_parent', 'reference_type_val_parent', 'integer',array('integer' => 'Parent ID sholud be integer value only'));


        $this->form_validation->set_rules('reference_type_is_active', 'reference_type_is_active', 'required|integer|regex_match[/^[0-1]$/]', array(
'required' => $error_label.' Status sholud not be blank','integer' => $error_label.' Status sholud be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
));

        if ($this->form_validation->run() == FALSE)
            {


            foreach($this->form_validation->error_array() as $formerrors_key => $formerrors_value){


            $this->response([
            'status' => FALSE,
            'message' => $formerrors_value,
            'data' => $formerrors_value
            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

            }


            }


        
        $reference_type_val=$this->post('reference_type_val');
        
        $reference_type_is_active=$this->post('reference_type_is_active');;

        $data=array();

        $data['reference_type_id']=$reference_type_id;
        $data['reference_type_val']=$reference_type_val;
        $data['reference_type_val_parent']=$reference_type_val_parent;
        $data['reference_type_is_active']=$reference_type_is_active;


        $res_add=$this->References_model->AddReferenceValue($data);

        

        if($res_add){
            //sucess

            $this->response([
                    'status' => TRUE,
                    'message' => $error_label.' Added Successfully',
                    'data' => $error_label.' Added Successfully'
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }
        else{

            $this->response([
                    'status' => FALSE,
                    'message' => $error_label.' Adding Failed',
                    'data' => $error_label.' Adding Failed'
                ], REST_Controller::HTTP_NOT_FOUND); // OK (200) being the HTTP response code

            //fail

        }
    }
    public function referenceUpdate_post(){


        $reference_type_val_id=$this->post('reference_type_val_id');


        //$reference_type_id=$this->post('reference_type_id');

        $reference_type_val_parent=empty(trim($this->post('reference_type_val_parent'))) ? '' : trim($this->post('reference_type_val_parent'));
        $error_label="Reference";
        /* $reftype=$this->References_model->GetReferences($reference_type_id);
        foreach($reftype as $rowrefurenece){

            $error_label=$rowrefurenece->name;

        } */


        /* $reftype_values=$this->References_model->GetReferenceValues($reference_type_val_parent);
        foreach($reftype_values as $rowrefurenece_values){

            $parent_reftypeid=$rowrefurenece_values->parent_id;

            $reftype_parent=$this->References_model->GetReferences($parent_reftypeid);

            foreach($reftype_parent as $rowrefurenece_parent){

            $error_label_parent=$rowrefurenece->name;

            }

        } */

        
        $this->form_validation->set_rules('reference_type_val_id', 'reference_type_val_id', 'required|integer',array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        ));

        
        /*$this->form_validation->set_rules('reference_type_id', 'reference_type_id', 'required|integer',array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        ));*/


        $this->form_validation->set_rules('reference_type_val', $error_label.' Value', 'required|regex_match[/^[A-Za-z,\']+$/]',array(
        'required' => $error_label.' Value sholud not be blank','regex_match' => $error_label.' Value sholud be alphabetic only'
        ));

        /*
        $this->form_validation->set_rules('reference_type_val_parent', 'reference_type_val_parent', 'integer',array('integer' => 'Parent ID sholud be integer value only'));
        */


        $this->form_validation->set_rules('reference_type_is_active', 'reference_type_is_active', 'required|integer|regex_match[/^[0-1]$/]', array(
'required' => $error_label.' Status sholud not be blank','integer' => $error_label.' Status sholud be integer value only','regex_match' => $error_label.' Status should be either 0 or 1'
));

        if ($this->form_validation->run() == FALSE)
            {


            foreach($this->form_validation->error_array() as $formerrors_key => $formerrors_value){


            $this->response([
            'status' => FALSE,
            'message' => $formerrors_value,
            'data' => $formerrors_value
            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

            }


            }


        
        $reference_type_val=$this->post('reference_type_val');
        
        $reference_type_is_active=$this->post('reference_type_is_active');;

        $data=array();

        //$data['reference_type_id']=$reference_type_id;
        $data['reference_type_val']=$reference_type_val;
        //$data['reference_type_val_parent']=$reference_type_val_parent;
        $data['reference_type_is_active']=$reference_type_is_active;
        $data['reference_type_val_id']=$reference_type_val_id;

        


        $res_update=$this->References_model->UpdateReferenceValue($data);

        

        if($res_update){
            //sucess

            $this->response([
                    'status' => TRUE,
                    'message' => $error_label.' Updated Successfully',
                    'data' => $error_label.' Updated Successfully'
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }
        else{

            $this->response([
                    'status' => FALSE,
                    'message' => $error_label.' Updation Failed',
                    'data' => $error_label.' Updation Failed'
                ], REST_Controller::HTTP_NOT_FOUND); // OK (200) being the HTTP response code

            //fail

        }
    }
    public function reference_delete(){

        $error_label="Reference";

        $reference_type_val_id=$this->get('reference_type_val_id');

        $this->form_validation->set_data(array("reference_type_val_id" => $this->get('reference_type_val_id')));

        $this->form_validation->set_rules('reference_type_val_id', 'reference_type_val_id', 'required|integer',array(
        'required' => $error_label.' ID sholud not be blank','integer' => $error_label.' ID sholud be integer value only'
        ));

        if ($this->form_validation->run() == FALSE)
        {


            foreach($this->form_validation->error_array() as $formerrors_key => $formerrors_value){


            $this->response([
            'status' => FALSE,
            'message' => $formerrors_value,
            'data' => $formerrors_value
            ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

            }


        }

        $data['reference_type_val_id']=$reference_type_val_id;


        $res_delete=$this->References_model->RemoveReferenceValue($data);

        

        if($res_delete){
            //sucess

            $this->response([
                    'status' => TRUE,
                    'message' => $error_label.' Deleted Successfully',
                    'data' => $error_label.' Deleted Successfully'
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        }
        else{

            $this->response([
                    'status' => FALSE,
                    'message' => $error_label.' Deletion Failed',
                    'data' => $error_label.' Deletion Failed'
                ], REST_Controller::HTTP_NOT_FOUND); // OK (200) being the HTTP response code

            //fail

        }

    }
}
