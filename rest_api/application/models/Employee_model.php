<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model
{
     public function __construct()
    {
        parent::__construct();

    }
    public function getEmployeeCount($data){
        
        $this->db->select('employee_id');
        if ($data['employe_id'] !== NULL)
        {
            $this->db->where('employee_id', $data['employe_id']);
        }
        if ($data['search_key']!== NULL)
        {
            $this->db->where($data['search_key'], $data['search_value']);
        }
        
        $query = $this->db->get('employee');
        if(!$query){
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
                else{
                    
                    $db_response=array("status"=>true,"message"=>'success',"data"=>array("count"=>$query->num_rows()));
                    
                    
                }
                return $db_response;
       
        
    }
    public function getEmployeeList($data,$limit=0, $start=0){
        
        $db_response=array();
        try{ 
                $this->db->select('employee_id,employee_code,email');
                if ($data['employe_id'] !== NULL)
                {
                    $this->db->where('employee_id', $data['employe_id']);
                }
                if ($data['search_key']!== NULL)
                {
                    $this->db->where($data['search_key'], $data['search_value']);
                }
                    $this->db->limit($limit, $start);

                    $query = $this->db->get('employee');

                if(!$query){
                    $error = $this->db->error(); 
                    $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                }
                else{
                    if ($query->num_rows() > 0)
                    {
                        $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
                    }
                    else{
                        $db_response=array("status"=>false,"message"=>'No records found',"data"=>array());
                    }
                }
        }
        catch(Exception $ex){
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        return $db_response;
     }
    public function addEmployee($data){
        try{
         $data_insert = array(
        'employee_code' => $data['employee_code'] ,
        'email' => $data['employee_mailid']);
        
        $res_insert=$this->db->insert('employee', $data_insert);
        if($res_insert){
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
         }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        return $db_response;
        
    }
    public function updateEmployee($data){
        
        try{
            $data_update = array(
        'employee_code' => $data['employee_code'] ,
        'email' => $data['employee_mailid']);
        
        $this->db->where('employee_id', $data['employee_id']);
        $res_update=$this->db->update('employee', $data_update);
        if($res_update){
            
            
            $db_response=array("status"=>true,"message"=>"success","data"=>array());
            
        }
        else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        
        return $db_response;
        
    }
    public function deleteEmployee($id){
        
        $db_response=array("status"=>true,"message"=>"success","data"=>array());
        return $db_response;
        
    }
}  

?>

