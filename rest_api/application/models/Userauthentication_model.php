<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userauthentication_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();

    }
    function checkUserExist($data){
        
        $this->db->trans_begin();
        $this->db->select('user_id, login_id, role');
        $this->db->where('login_id', $data['user_email_id']);
        $query = $this->db->get('user');

        if($query){
            if ($query->num_rows() > 0)
            {
                foreach($query->result() as $k=>$v){
                    if($v->user_id){
                        
                        if($v->role==2){
                            $data_update=array();
                            if(!empty($data['user_name'])){
                                $data_update['name']=$data['user_name'];
                            }
                            if(!empty($data['phone_number'])){
                                $data_update['phone']=$data['phone_number'];
                            }
                            if(!empty($data['profile_image'])){
                                $data_update['image']=$data['profile_image'];
                            }
                            if(!empty($data['user_dob'])){
                                $data_update['dob']=date('Y-m-d', strtotime($data['user_dob']));
                            }
                            
                            
                            if(count($data_update)>0){
                            $this->db->where('email', $data['user_email_id']);
                            $res_update=$this->db->update('employee', $data_update);
                            }
                            if($res_update || count($data_update)==0){
                                $db_response=array("status"=>true,"message"=>"success","data"=>array('id' => $v->user_id, 'email' => $v->login_id, 'role' => $v->role));
                            }
                            else{
                                //updation failed
                                $error = $this->db->error(); 
                                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                            }
                            
                        }
                        else{
                            
                            $db_response=array("status"=>true,"message"=>"success","data"=>array('id' => $v->user_id, 'email' => $v->login_id, 'role' => $v->role));
                        }
                        
                        
                    }else{
                        $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
                    }
                }
                
            }
            else{
                
                $db_response=array("status"=>false,"message"=>"No User Found!","data"=>array());
            }
        }else{
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
            $this->db->trans_complete();
            
            
            if ($this->db->trans_status() === FALSE)
            {
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }

        return $db_response;
        
    }
    
}

?>

