<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class References_model extends CI_Model
{

    var $referencestypes=array();
    public function __construct()
    {
        parent::__construct();

    }

    public function GetReferences($id = NULL)
    {

        $this->db->select('*');

        if ($id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $id);
        }


        $query = $this->db->get('reference_type');

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else{
            return FALSE;
        }

    }
    public function getReferenceValuesList($id = NULL){


        
        try{
            $query = $this->db->query("call getReferenceValueListByReferencetype($id)");
        if(!$query){
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if ($query->num_rows() > 0)
            {
                
                $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
            }
            else{
                $db_response=array("status"=>false,"message"=>'No records found',"data"=>array());
            }
            
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
            return $db_response;
        
        
    }
    public function getAddReferenceValueFileds_main($id){

        $this->db->select('reference_type_id,name,parent_id,order');
       
        if ($id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $id);
        }
        $query = $this->db->get('reference_type');
        
        if ($query->num_rows() > 0)
        {

            $qresult=$query->result();
            foreach($qresult as $qresultrow){
                
                $this->referencestypes[]=$qresultrow;
                
                if($qresultrow->parent_id!=''){

                   $this->getAddReferenceValueFileds_main($qresultrow->parent_id);


                }
                else{
                    //return $this->referencestypes[];
                }
                
            }
            
        }

    }
    public function getAddReferenceValueFileds($id){

        $this->referencestypes=array();
        $this->getAddReferenceValueFileds_main($id);
        return $this->referencestypes;
        

    }
    public function getReferenceValues($reference_type_id=NULL,$parent_id=NULL){

        try{
            $this->db->select('reference_type_value_id,value');
       
        if ($reference_type_id === NULL)
        {

        }
        else{
         $this->db->where('reference_type_id', $reference_type_id);
        }
        if ($parent_id === NULL)
        {

        }
        else{
         $this->db->where('parent_id', $parent_id);
        }

         $this->db->where('is_active', 1);

        $query = $this->db->get('reference_type_value');
        if(!$query){
            $error = $this->db->error(); 
            $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
        }
        else{
            if ($query->num_rows() > 0)
        {
           
            $db_response=array("status"=>true,"message"=>'success',"data"=>$query->result());
        }
        else{
            $db_response=array("status"=>false,"message"=>'No Records Found',"data"=>array());
        }
        }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        return $db_response;
        
        


    }

    public function AddReferenceValue($data){

        //return $data;

        //$insert_array
        try{
            $data_insert = array(
            'reference_type_id' => $data['reference_type_id'] ,
            'value' => $data['reference_type_val'] ,
            'fk_created_by' => 1 ,
            'created_date' => date("Y-m-d H:i:s"),
            'parent_id' => $data['reference_type_val_parent'],
            'is_active' => $data['reference_type_is_active']
            );


            $res_insert=$this->db->insert('reference_type_value', $data_insert);
            if($res_insert){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        
        return $db_response;



    }
    function UpdateReferenceValue($data){


        try{
            $data_update = array(
            'reference_type_id' => $data['reference_type_id'] ,
            'value' => $data['reference_type_val'] ,
            'fk_created_by' => 1 ,
            'created_date' => date("Y-m-d H:i:s"),
            'parent_id' => $data['reference_type_val_parent'],
            'is_active' => $data['reference_type_is_active']
            );

            $this->db->where('reference_type_value_id', $data['reference_type_val_id']);
            $res_update=$this->db->update('reference_type_value', $data_update);
            if($res_update){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
            $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }

        return $db_response;



    }
    function RemoveReferenceValue($data){

        try{
            $data_update = array(
            'is_active' => 0
            );

            $this->db->where('reference_type_value_id', $data['reference_type_val_id']);
            $res_update=$this->db->update('reference_type_value', $data_update);
            if($res_update){
                $db_response=array("status"=>true,"message"=>'success',"data"=>array());
            }
            else{
                $error = $this->db->error(); 
                $db_response=array("status"=>false,"message"=>$error['message'],"data"=>array());
            }
        } catch (Exception $ex) {
                $db_response=array("status"=>false,"message"=>'Exception',"data"=>array());
        }
        return $db_response;

    }
    

}
