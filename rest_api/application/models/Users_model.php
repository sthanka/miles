<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function createUser($data) {
//            print_r($data);die;
        $data_insert = array(
            'name' => $data['name']
        );

        $res_insert = $this->db->insert('user', $data_insert);
        if ($res_insert) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteUser($data) {
        $data_update = array("status" => $data['status']);

        $this->db->where('uid', $data['id']);
        $res_delete = $this->db->update('user', $data_update);

        if ($res_delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserStatus($id) {
        $sql = "SELECT status FROM user WHERE uid = ?";
        $res_all = $this->db->query($sql, array($id))->result();

        if ($res_all && count($res_all) > 0) {
            return $res_all[0];
        } else {
            return false;
        }
    }

    public function updateUser($data) {
        $data_update = array();
        if (isset($data['role'])) {
            $data_update = array("role" => $data['role']);
        }

        if (isset($data['name'])) {
            $data_update = array("name" => $data['name']);
        }

        $this->db->where('uid', $data['id']);
        $res_update = $this->db->update('user', $data_update);

        if ($res_update) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllUser($id = NULL, $limit = NULL, $start = NULL) {
        if ($id === NULL) {
            //$res = $this->db->get('user');
            $this->db->select('*');
            $this->db->from('user');
            if ($limit !== NULL && $start !== NULL) {
                if ($limit != 1) {
                    $limit--;
                }  //Bug since if we pass 2 the pointer starts from 3
                $this->db->limit($start, $limit);
            }
            $res_all = $this->db->get()->result();
            if ($res_all) {
                return $res_all;
            } else {
                return false;
            }
        } else {
            $sql = "SELECT * FROM user WHERE uid = ?";
            $res_all = $this->db->query($sql, array($id))->result();

//            $res_all=$this->db->get('user')->result();
            if ($res_all) {
                return $res_all;
            } else {
                return false;
            }
        }
    }

    public function paginatedUser($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("user");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /**/

    public function selectUser($id = NULL) {

        $this->db->select('*');
        if ($id === NULL) {
            
        } else
            $this->db->where('id', $id);

        $this->db->where('role', 2);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function selectAdmin($id = NULL) {

        $this->db->select('*');
        if ($id === NULL) {
            
        } else
            $this->db->where('id', $id);

        $this->db->where('role', 1);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function registeruser($data) {

        $data_insert = array(
            'name' => $data['fullname'],
            'email' => $data['email'],
            'password' => $data['password'],
            'created_at' => date("Y-m-d H:i:s"),
            'role' => '2'
        );
        
        $res_insert = $this->db->insert('users', $data_insert);
        if ($res_insert) {

            $user_id = $this->db->insert_id();
            $client_name = $data['email'];
            $client_secret = base64_encode($data['password']);

            $data_client_create = array(
                "user_id" => $user_id,
                "secret" => $client_secret,
                "name" => $client_name,
                "created_at" => date('Y-m-d H:i:s'));
            $res_insert_client = $this->db->insert('oauth_clients', $data_client_create);

            if ($res_insert_client) {

                $res_client_id = $this->db->insert_id();
                $this->db->select('scope_id')->where('role_id', 2);
                $query = $this->db->get('oauth_scopes_roles');
                $enabledscopes = $query->result();

                foreach ($enabledscopes as $clientscopes) {

                    $scopes = array();
                    $scopes = array(
                        "client_id" => $res_client_id,
                        "scope_id" => $clientscopes->scope_id,
                        "created_at" => date("Y-m-d H:i:s"));
                    $res_insert_client_scopes = $this->db->insert('oauth_client_scopes', $scopes);
                }

                return true;
            } else {

                return false;
            }
        } else {
            return false;
        }
    }

    public function insertUser($data) {
        
        $data_insert = array(
            'full_name' => $data['name'],
            'profile_pic' => $data['profile_pic'],
            'status' => 1
        );

        $res_insert = $this->db->insert('users', $data_insert);
        if ($res_insert) {
            return true;
        } else {
            return false;
        }
    }

    public function updateAUser() {
        
    }

    public function deleteAUser($id) {
        
        $data_update = array("status" => "0");
        $this->db->where('id', $id);
        $res_delete1 = $this->db->update('users', $data_update);


        $data_update = array("status" => "0");
        $this->db->where('user_id', $id);
        $res_delete2 = $this->db->update('oauth_clients', $data_update);

        if ($res_delete1 && $res_delete2) {
            return true;
        } else {
            return false;
        }
    }

    public function activateUser($id) {

        $data_update = array("status" => "1");
        $this->db->where('id', $id);
        $res_delete1 = $this->db->update('users', $data_update);


        $data_update = array("status" => "1");
        $this->db->where('user_id', $id);
        $res_delete2 = $this->db->update('oauth_clients', $data_update);

        if ($res_delete1 && $res_delete2) {
            return true;
        } else {
            return false;
        }
    }

    public function loginUser($data) {


        $this->db->select('*');
        $this->db->where('email', $data['username']);
        $this->db->where('password', $data['password']);

        $query = $this->db->get('users');

        if($query){
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return FALSE;
            }
        }else{
            return 'dberror';
        }
    }

    public function getsecret_id($client_id = '') {

        $this->db->select('secret');
        $this->db->where('user_id', $client_id);
        $query = $this->db->get('oauth_clients');
        
        if($query){
            if ($query->num_rows() > 0) {
                $secretcodes = $query->result();
                return $secretcodes[0]->secret;
            } else {
                return FALSE;
            }
        }else{
            return 'dberror';
        }
    }

    public function getscopes($client_id = '') {

        $this->db->select('scope_id');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get('oauth_client_scopes');
        
        if($query){
            if ($query->num_rows() > 0)
            {
                return $query->result();
            }else{
                return FALSE;
            }
        }else{
            return 'dberror';
        }
    }

    /**/
}
